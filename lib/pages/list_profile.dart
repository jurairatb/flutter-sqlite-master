

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:sqlite_basic/database/database_helper.dart';
import 'package:sqlite_basic/model/profile_model.dart';
import 'package:sqlite_basic/pages/add_profile.dart';
import 'package:sqlite_basic/pages/edit_profile.dart';
import 'package:sqlite_basic/pages/show_profile.dart';

class ListProfile extends StatefulWidget {
  const ListProfile({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<ListProfile> createState() => _ListProfileState();
}

class _ListProfileState extends State<ListProfile> {

  int? selectedId;

  @override
  void initState(){
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: [
          Padding(
            padding: EdgeInsets.only(right: 20.0),
            child: IconButton(
              icon: const Icon(Icons.add),
              tooltip: 'Add Profile',
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => AddProfile()),
                ).then((value){
                  //getAllData();
                  setState(() {

                  });
                });
              },

            ),
          ),
        ],
      ),

      body: Center(
        child: FutureBuilder<List<ProfileModel>>(
            future: DatabaseHelper.instance.getProfiles(),
            builder: (BuildContext context,
                AsyncSnapshot<List<ProfileModel>> snapshot) {
              if (!snapshot.hasData) {
                return Center(child: Text('Loading...'));
              }
              return snapshot.data!.isEmpty
                  ? Center(child: Text('No Profiles in List.'))
                  : ListView(
                children: snapshot.data!.map((grocery) {
                  return Center(
                    child: Card(
                      color: selectedId == grocery.id
                          ? Colors.white70
                          : Colors.white,
                      child: ListTile(
                        title: Text('${grocery.firstname} ${grocery.lastname}'),
                        subtitle: Text(grocery.email),
                        leading: CircleAvatar(backgroundImage: FileImage(File(grocery.image))),
                        //leading: Image(
                        //  image: FileImage(File(grocery.image)),
                        //  fit: BoxFit.cover,
                        //  height: 500,
                        //  width: 100,
                        //),
                        trailing: Row(
                          mainAxisSize: MainAxisSize.min,
                          children:  <Widget>[
                            new IconButton(
                              padding: EdgeInsets.all(0),
                              icon: Icon(Icons.edit),
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (context) => EditProfile(grocery)),
                                ).then((value){
                                  setState(() {
                                  });
                                });
                              },
                            ),
                            new IconButton(
                              padding: EdgeInsets.all(0),
                              icon: Icon(Icons.clear),
                              onPressed: () {
                                showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return AlertDialog(
                                      title: new Text("Do you want to delete this record?"),
                                      // content: new Text("Please Confirm"),
                                      actions: [
                                        new TextButton(
                                          onPressed: () {
                                            DatabaseHelper.instance.remove(grocery.id!);
                                            setState(() {
                                              Navigator.of(context).pop();
                                            });
                                          },
                                          child: new Text("Ok"),
                                        ),
                                        Visibility(
                                          visible: true,
                                          child: new TextButton(
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                            child: new Text("Cancel"),
                                          ),
                                        ),
                                      ],
                                    );
                                  },
                                );

                              },
                            ),
                          ],
                        ),
                        onTap: () {
                          var profileid = grocery.id;
                          Navigator.push(context, MaterialPageRoute(builder: (context) => ShowProfile(id: profileid)));

                          setState(() {
                            print(grocery.image);
                            if (selectedId == null) {
                              //firstname.text = grocery.firstname;
                              selectedId = grocery.id;
                            } else {
                              // textController.text = '';
                              selectedId = null;
                            }
                          });
                        },
                        onLongPress: () {
                          setState(() {
                            DatabaseHelper.instance.remove(grocery.id!);
                          });
                        },
                      ),
                    ),
                  );
                }).toList(),
              );
            }),
      ),
    );
  }
}